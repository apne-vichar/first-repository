import React,{ Component } from "react";

export default class Mobile extends Component
{
    constructor()
    {
        super();
        this.state={
            showHiddenDetails:false
        }
    }
    hiddenDetails=()=>
    {
        return (<>
            <span>Ram: {this.props.mobile.ram}</span><br />
            <span>Camera: {this.props.mobile.camera}</span><br />
            <span>OS: {this.props.mobile.os}</span><br />
            <span>Processor: {this.props.mobile.processor}</span><br />
        </>
        )
    }
    render()
    {
        return(
            <div className="card m-2" style={{ width: 300 }}>
                <div className="card-body">
                    <p className="card-text">
                        <span>Model: {this.props.mobile.model}</span><br />
                        <span>Price: {this.props.mobile.price}</span><br /><br/>
                        {this.state.showHiddenDetails?this.hiddenDetails():""}
                    </p>
                    <div><i></i></div><br />
                    <button className="btn btn-danger" 
                    onClick={()=>this.state.showHiddenDetails==false?this.setState({showHiddenDetails:true}):this.setState({showHiddenDetails:true})}
                    >ShowDetails</button>
                </div>

            </div>
        );
    }
}